﻿#include <iostream>
#include "functions.hpp"

namespace saw
{
	DoubleList::DoubleList()
	{
		m_sent = new D_List;
		m_sent->next = m_sent;
		m_sent->prev = m_sent;

		head = nullptr;
		tail = nullptr;
		count = 0;
	}


	void DoubleList::ADDINHEAD(int data)
	{
		D_List* tmp = new D_List;

		tmp->next = m_sent->next;
		tmp->prev = m_sent;
		m_sent->next->prev = tmp;
		m_sent->next = tmp;
		tmp->data = data;
		count++;
	}

	void DoubleList::ADDINTAIL(int data)
	{
		D_List* tmp = new D_List;

		tmp->prev = m_sent->prev;
		tmp->next = m_sent;
		m_sent->prev->next = tmp;
		m_sent->prev = tmp;
		tmp->data = data;

		count++;
	}

	void DoubleList::PRINT()
	{
		if (count != 0)
		{
			D_List* tmp = m_sent->next;
			while (tmp != m_sent)
			{
				std::cout << tmp->data << " , ";
				tmp = tmp->next;
			}
		}
	}

	void DoubleList::REVERSEPRINT()
	{
		if (count != 0)
		{
			D_List* tmp = m_sent->prev;
			while (tmp != m_sent)
			{
				std::cout << tmp->data << " , ";
				tmp = tmp->prev;
			}
		}
	}


	void DoubleList::INSERT(int pos, int data)
	{
		if (pos < 1 || pos > count + 1) { std::cout << "Incorrect position! Try again! "; std::cin >> pos; }

		if (pos == count + 1)
		{
			ADDINTAIL(data);
			return;
		}

		if (pos == 1)
		{
			ADDINHEAD(data);
			return;
		}

		int i = 0;
		D_List* curr = m_sent;

		while (i < pos)
		{
			curr = curr->next;
			i++;
		}

		D_List* prevcurr = curr->prev;

		D_List* tmp = new D_List;

		tmp->data = data;

		if (prevcurr != nullptr && count != 1)
			prevcurr->next = tmp;

		tmp->next = curr;
		tmp->prev = prevcurr;
		curr->prev = tmp;

		count++;
	}

	void DoubleList::DELETE(int pos)
	{
		if (pos < 1 || pos > count) { std::cout << "Incorrect position! Try again! "; std::cin >> pos; }

		int i = 1;

		D_List* Del = m_sent->next;

		while (Del != m_sent && i < pos)
		{
			Del = Del->next;
			i++;
		}

		Del->next->prev = Del->prev;
		Del->prev->next = Del->next;
		delete Del;

		count--;
	}

	void DoubleList::CLEAR()
	{
		D_List* tmp;
		D_List* p = m_sent->next;
		while (p != m_sent)
		{
			tmp = p;
			p = p->next;
			delete tmp;
		}
	}

	DoubleList::~DoubleList()
	{
		CLEAR();
		delete m_sent;
	}
};