#include <iostream>
#include "functions.hpp"

using namespace saw;

int main()
{
	DoubleList list;

	int a[4] = { 4, 13, 54, 23 };

	for (int i = 0; i < 4; i++)
	{
		if (a[i] % 2 == 0)
			list.ADDINHEAD(a[i]);
		else
			list.ADDINTAIL(a[i]);

	}
	std::cout << std::endl;

	//list.INSERT(1, 7);
	//list.DELETE(3);
	list.PRINT();
	std::cout << std::endl;
	list.REVERSEPRINT();

	return 0;

}